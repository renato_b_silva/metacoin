package br.com.capela.app1.service;

import java.io.IOException;
import java.util.Comparator;

import org.springframework.stereotype.Service;

import br.com.capela.app1.model.GameInfo;
import br.com.capela.app1.model.observable.SaveCoinsObservable;
import rx.Observable;
import rx.schedulers.Schedulers;

@Service
public class GameInfoService {
	
	private Observable<GameInfo> b;
	
	public GameInfoService() throws IOException {
		SaveCoinsObservable sco = new SaveCoinsObservable();
		b = sco
			.getObservable()
			.subscribeOn(Schedulers.io())
			.cache();
	}
	
	public void consulta(final int limit) throws IOException {
		b	
			.take(limit)
			.toList()
			.subscribe(games -> games.stream().sorted(Comparator.reverseOrder()).forEach(System.out::println));
	}
}

package br.com.capela.app1.model.observable;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson.JacksonFactory;

import br.com.capela.app1.model.GameInfo;
import br.com.capela.app1.model.MetacriticGameInfo;
import br.com.capela.app1.model.SaveCoinGameInfo;
import br.com.capela.app1.model.SaveCoinGameInfo.SaveCoinUrl;
import rx.Observable;

public class SaveCoinsObservable {
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveCoinsObservable.class);
	private static final String URL_SAVECOIN = "https://api.savecoins.me/v1/games?currency=BRL&locale=pt&filter[title]=";
	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	private static final JsonFactory JSON_FACTORY = new JacksonFactory();
	
	private Observable<GameInfo> observable;
	
	public SaveCoinsObservable() throws IOException{
		MetaCriticObservable metaCriticObservable = new MetaCriticObservable();
		Observable<MetacriticGameInfo> mcgiObserver = metaCriticObservable.getObservable();
		
		observable = Observable.using(
			() -> {  
				try {
					return HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
						@Override
						public void initialize(HttpRequest request) {
							request.setParser(new JsonObjectParser(JSON_FACTORY));
						}
					});
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}, requestFactory -> {
				return mcgiObserver.flatMap(mcgi -> {
					try {
						SaveCoinUrl url = new SaveCoinUrl(URL_SAVECOIN + mcgi.getName());
						HttpRequest request = requestFactory.buildGetRequest(url);
						SaveCoinGameInfo saveCoinGame = request.execute().parseAs(SaveCoinGameInfo.class);
						return Observable
								.just(new GameInfo(mcgi, saveCoinGame))
								.filter(game -> !game.price().equals(Float.NaN));
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				});
			}, requestFactory -> {LOGGER.debug("Recursos liberados"); requestFactory = null;});
	}
	
	public Observable<GameInfo> getObservable() {
		return observable;
	}
}

package br.com.capela.app1.model.observable;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.capela.app1.model.MetacriticGameInfo;
import rx.Observable;

public class MetaCriticObservable {
	private static final Logger LOGGER = LoggerFactory.getLogger(MetaCriticObservable.class);
	private static final String URL_METACRITIC = "http://www.metacritic.com/browse/games/score/metascore/all/switch/filtered?sort=desc";
	
	private Observable<MetacriticGameInfo> observable;
	
	public MetaCriticObservable() throws IOException{
		observable = Observable
			.using(
				() -> {  
					try {
						Document doc = Jsoup.connect(URL_METACRITIC).get();
						Elements allElementGames = doc.getElementsByClass("product_row");
						LOGGER.debug("Elementos carregados");
						return allElementGames;
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				}, allMetaGames -> {
					return Observable
							.from(allMetaGames)
							.flatMap(metaGame -> 
								Observable.just(new MetacriticGameInfo(
									metaGame.getElementsByClass("product_title").get(0).text(),
									metaGame.getElementsByClass("row_num").get(0).text().replace(".", ""),
									metaGame.getElementsByClass("product_score").get(0).text()))
								);
				}, allMetaGames -> {LOGGER.debug("Elementos liberados"); allMetaGames.clear(); })
			;
	}
	
	public Observable<MetacriticGameInfo> getObservable() {
		return observable;
	}
}

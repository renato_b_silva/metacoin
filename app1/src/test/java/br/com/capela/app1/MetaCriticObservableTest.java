package br.com.capela.app1;

import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Test;

import br.com.capela.app1.model.MetacriticGameInfo;
import br.com.capela.app1.model.observable.MetaCriticObservable;
import rx.Observable;
import rx.observables.ConnectableObservable;

public class MetaCriticObservableTest {
	
	@Test
	public void getObservable() throws Exception{
		List<MetacriticGameInfo> _games = Lists.newArrayList(); 
		
		MetaCriticObservable mco = new MetaCriticObservable();
		Observable<MetacriticGameInfo> o = mco.getObservable().cacheWithInitialCapacity(5);
		System.out.println(o);
		ConnectableObservable<MetacriticGameInfo> o2 = o.take(5).publish();
		o2.subscribe(game -> System.out.println(game.getMetacrictPosition() +" "+game.getName()));
		o2.subscribe(game -> _games.add(game));
		
		o2.connect();
		System.out.println(_games);
	}
	
}

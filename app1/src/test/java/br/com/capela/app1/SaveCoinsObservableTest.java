package br.com.capela.app1;

import java.util.Comparator;

import org.junit.Test;

import br.com.capela.app1.model.GameInfo;
import br.com.capela.app1.model.observable.SaveCoinsObservable;
import rx.Observable;

public class SaveCoinsObservableTest {
	
	@Test
	public void getObservable() throws Exception{
		SaveCoinsObservable sco = new SaveCoinsObservable();
		Observable<GameInfo> b = sco.getObservable();
		
		b.
			filter(game -> !game.price().equals(Float.NaN)).
			take(50).
			toList().
			subscribe(games -> 
				games.stream().sorted(Comparator.reverseOrder()).forEach(System.out::println)
			);
	}
	
}
